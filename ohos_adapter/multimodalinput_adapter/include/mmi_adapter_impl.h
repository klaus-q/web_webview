/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef MMI_ADAPTER_IMPL_H
#define MMI_ADAPTER_IMPL_H

#include "mmi_adapter.h"

#include "input_manager.h"

namespace OHOS::NWeb {
class MMIListenerAdapterImpl : public MMI::IInputDeviceListener {
public:
    MMIListenerAdapterImpl(std::shared_ptr<MMIListenerAdapter> listener);

    ~MMIListenerAdapterImpl();

    void OnDeviceAdded(int32_t deviceId, const std::string &type) override;

    void OnDeviceRemoved(int32_t deviceId, const std::string &type) override;

private:
    std::shared_ptr<MMIListenerAdapter> listener_ = nullptr;
};

class MMIAdapterImpl : public MMIAdapter {
public:
    MMIAdapterImpl() = default;

    ~MMIAdapterImpl() = default;

    int32_t RegisterDevListener(std::string type, std::shared_ptr<MMIListenerAdapter> listener) override;

    int32_t UnregisterDevListener(std::string type) override;

    int32_t GetKeyboardType(int32_t deviceId, std::function<void(int32_t)> callback) override;

    int32_t GetDeviceIds(std::function<void(std::vector<int32_t>&)> callback) override;

private:
    std::shared_ptr<MMI::IInputDeviceListener> devListener_ = nullptr;
};
}  // namespace OHOS::NWeb

#endif // MMI_ADAPTER_IMPL_H